function tinhTien(){
    var ten = document.getElementById("txt-ten").value ;
    var soDien = document.getElementById("txt-so-dien").value *1;
    var tienDien = tinhTienDien(soDien);
    document.getElementById("result").innerHTML = `Tiền điện của ${ten} là ${tienDien}`;
}
function tinhTienDien(soDien){
    var tienDien = 0;
    if(soDien <= 50){
        tienDien = soDien * 500;
    }
    else if (soDien <= 100){
        tienDien = 50*500 + (soDien-50)*650;
    } else if(soDien <= 200){
        tienDien = 50*500 + 50*650 + (soDien-100)*850;
    } else if(soDien <= 350){
        tienDien = 50*500 + 50*650 + 100*850 + (soDien - 200)*1100;
    }else{
        tienDien = 50*500 + 50*650 + 100*850 + 150*1100 + (soDien - 350)*1300;
    }
    return tienDien;
}