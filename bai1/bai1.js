function ketQua(){
    var diem1 = document.getElementById("txt-diem-1").value *1;
    var diem2 = document.getElementById("txt-diem-2").value *1;
    var diem3 = document.getElementById("txt-diem-3").value *1;
    var diemChuan = document.getElementById("txt-diem-chuan").value *1;
    var khuVuc = document.getElementById("txt-khu-vuc").value *1;
    var doiTuong = document.getElementById("txt-doi-tuong").value *1;
    var kQ = "";
    var diemTK = tinhDiemTK(diem1,diem2,diem3,khuVuc,doiTuong);

    if((diem1 == 0) || (diem2 == 0) || (diem3 == 0)){
        kQ = `Rất tiếc. Bạn không trúng tuyền vì có môn 0 điểm.`
    }
    else{
        kQ = xetTuyen(diemChuan,diemTK);
    }
    document.getElementById("result").innerHTML = `${kQ} Tổng điểm: ${diemTK}`;

}
function tinhDiemTK(mon1,mon2,mon3,khuVuc,doiTuong){
    var diemTK = mon1 + mon2 +mon3 +khuVuc +doiTuong;
    return diemTK;
}
function xetTuyen(diemChuan,diemTK){
    var kQ ="";
    if(diemChuan <= diemTK){
        kQ = `Chúc mừng bạn đã trúng tuyển.`;
    }else{
        kQ = `Rất tiếc. Bạn không trúng tuyển.`;
    }
    return kQ;
}